// route api
'use strict';

var apiRouter = require('express').Router();

var controller = require('../controller/controller');

apiRouter.post('/sum', controller.sum);

module.exports = apiRouter;