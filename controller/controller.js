// controller.js
'use strict';

var controller = {};

controller.sum = function(req, res){
    if(!req.body.firsNumber || !req.body.secondNumber) {
        res.status(404).json({
            flag: false,
            message: 'Please input your numbers'
        });
    } else {
        var firstNumber = req.body.firsNumber,
            secondNumber = req.body.secondNumber;

        var sum = parseFloat(firstNumber) + parseFloat(secondNumber);
       
        res.status(201).json({
            flag: true,
            message: 'Your sum is :' + sum
        });
    }
}

module.exports = controller;