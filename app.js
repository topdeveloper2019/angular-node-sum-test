// server.js
'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');


// Initialize application
const app = express();
const apiRouter = require('./route/api.js');

// Parse asn urlencoded and json
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Set static files location.
app.use(express.static(__dirname+  '/public'));

// Catch all routes.
// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, '/public/index.html'))
// });

app.use('/api', apiRouter);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/public/index.html'))
});

app.listen(3000, () => {
    console.log('Example app listening on port 3000!');
});

//Run app, then load http://localhost:3000 in a browser to see the output.