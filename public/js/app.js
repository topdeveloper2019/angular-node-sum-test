// Angular.js front-end application js
// ./public/js/app.js

var app = angular.module('testApp', []);

app.controller('testController', function($scope, $http) {
    $scope.calculateSum = function(){
        var sendData = {
            firsNumber: $scope.firstNumber,
            secondNumber: $scope.secondNumber
        }
        $http({
            method: 'POST',
            url: '/api/sum',
            data: sendData
        }).then(function(response) {
            console.log(response.data.message);

            alert(response.data.message)
        }).catch(function(error) { 
            alert(response.data.message)
        });
    }
});